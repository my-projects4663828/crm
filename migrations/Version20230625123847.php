<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230625123847 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create Company and Client tables';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE client (id INT AUTO_INCREMENT NOT NULL, picture_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, email VARCHAR(255) DEFAULT NULL, phone VARCHAR(255) DEFAULT NULL, INDEX IDX_C7440455EE45BDBF (picture_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client_company (client_id INT NOT NULL, company_id INT NOT NULL, INDEX IDX_1D824D3219EB6921 (client_id), INDEX IDX_1D824D32979B1AD6 (company_id), PRIMARY KEY(client_id, company_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company (id INT AUTO_INCREMENT NOT NULL, picture_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, email VARCHAR(255) DEFAULT NULL, phone VARCHAR(255) DEFAULT NULL, adress VARCHAR(255) DEFAULT NULL, INDEX IDX_4FBF094FEE45BDBF (picture_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C7440455EE45BDBF FOREIGN KEY (picture_id) REFERENCES media_object (id)');
        $this->addSql('ALTER TABLE client_company ADD CONSTRAINT FK_1D824D3219EB6921 FOREIGN KEY (client_id) REFERENCES client (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE client_company ADD CONSTRAINT FK_1D824D32979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094FEE45BDBF FOREIGN KEY (picture_id) REFERENCES media_object (id)');
        $this->addSql('ALTER TABLE user ADD picture_id INT DEFAULT NULL, ADD given_name VARCHAR(255) NOT NULL, ADD family_name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649EE45BDBF FOREIGN KEY (picture_id) REFERENCES media_object (id)');
        $this->addSql('CREATE INDEX IDX_8D93D649EE45BDBF ON user (picture_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C7440455EE45BDBF');
        $this->addSql('ALTER TABLE client_company DROP FOREIGN KEY FK_1D824D3219EB6921');
        $this->addSql('ALTER TABLE client_company DROP FOREIGN KEY FK_1D824D32979B1AD6');
        $this->addSql('ALTER TABLE company DROP FOREIGN KEY FK_4FBF094FEE45BDBF');
        $this->addSql('DROP TABLE client');
        $this->addSql('DROP TABLE client_company');
        $this->addSql('DROP TABLE company');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649EE45BDBF');
        $this->addSql('DROP INDEX IDX_8D93D649EE45BDBF ON user');
        $this->addSql('ALTER TABLE user DROP picture_id, DROP given_name, DROP family_name');
    }
}
